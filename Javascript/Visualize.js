function visualizeMatrix(matrix)
{
    // alustetaan cytoscape kirjasto
     cy = cytoscape({
        container: document.getElementById('cy'), // container to render in
         wheelSensitivity:0.1,
         style: [
            {
                selector: 'node',
                style: {
                    shape: 'hexagon',
                    'background-color': 'data(color)',
                    label: 'data(name)',
                    'font-size': 30
                }
            },
            {
                selector: 'edge',
                style: {
                    'target-arrow-color': '#ccc',
                    'target-arrow-shape': 'triangle',
                    'lineColor': 'data(color)',
                    'label': 'data(weight)',
                    'font-size': 30,

                }
            }
        ]

    });
     // lisätään nodet
    for (let i = 0; i < matrix.length; i++) {
        cy.add({
            group: 'nodes',
            data: { id:i.toString(), name: i.toString(),color: 'gray' },
            //     position: { x: 200, y: 200 }
        });
    }
    // lisätään polut
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix.length; j++) {
            if(matrix[i][j] != Infinity)
            {
                cy.add({
                    data: {
                        id: i + 'to' + j,
                        color: 'gray',
                        source: i.toString(),
                        target: j.toString(),
                        weight: matrix[i][j]
                    }
                });
            }
        }

    }
    // lisätään mahdollisuus poistaa polku hiiren painalluksella.
    cy.bind('tapstart', 'edge', function(event) {
        var connected = event.target.connectedNodes();
        var nodeone = connected[0]._private.data.id;
        var nodetwo = connected[1]._private.data.id;
        cy.remove('#' + nodeone + 'to' + nodetwo)
        cy.remove('#' + nodetwo + 'to' + nodeone)
        matrix[nodeone][nodetwo] = Infinity;
        matrix[nodetwo][nodeone] = Infinity;
    });
    // käytetään cytoscapen cose esitystapaa, joka on force-directed layout
    cy.layout({
        name: 'cose',
        padding: 30,
        randomize: false,
        gravity: 0.1,
        nodeRepulsion: function( node ){ return 20000000 },
        randomize: false,

    }).run();
}
// nollataan kaikki värit takaisin harmaaksi
function resetColors()
{
    var nodes = cy.$(':node');
    for (let node of nodes) {
        node.data('color','grey');
        node.data('name',node.data('id'));
    }
}
