// visualisoimisen delayhin liittyvä muuttuja
let c = 0;
let speed = 1000;

// Dijkstra funktio, ottaa parametrinä verkko matriisin, noden mistä aloitetaan, ja haluttaessa noden mihin lopetetaan
// Palauttaa matkat kaikkiin nodeihin tai polun, jos lopetusnode on määritelty.
function Dijkstra(Matrix, startNode, endNode = -1,s = 1000) {
    speed = s;
    // visualisoimisen delayhin liittyvä muuttuja
    c = 0;
    // Nodejen määrä = matriisin leveys/korkeus
    let nodeCount = Matrix.length;
    // visualisointi koodia
    highlightnode(startNode,'red');
    if(endNode != -1)
    {
        highlightnode(endNode,'red');
    }

    // alustetaan algoritmit tarvitsevat taulut.
    let foundNodes = new Array(nodeCount);
    // aloitusnode merkataan löydetyksi
    foundNodes[startNode] = true;
    let parents = new Array(nodeCount);
    //etäisyydet vastaavat aloitusnoden painoja alkuun.
    let distances = Array.from(Matrix[startNode]);
    // etäisyys aloitukseen = 0
    distances[startNode] = 0;
    // merkataan ensimmästen polkujen vanhemmiksi aloitusnode
    for (let i = 0; i < nodeCount; i++)
    {
        if (distances[i] < Infinity)
            parents[i] = startNode;
    }

    // päälooppi
    for (var i = 0; i < nodeCount - 1; i++) {
        var closestNode = -1;
        var distanceToClosestNode = Infinity;
        // tutkitaan löytämättömien nodejen etäisyydet ja valitaan lyhin.
        for (let j = 0; j < nodeCount; j++)
        {
            if (!foundNodes[j] && distances[j] < distanceToClosestNode)
            {
                distanceToClosestNode = distances[j];
                closestNode = j;
            }
        }
        highlight(parents[closestNode] , closestNode,distanceToClosestNode);
        // merkataan löydetyksi
        foundNodes[closestNode] = true;
        // jos lyhimmän matkan node on sama kuin lopetusnode lopetetaan.
        if(closestNode == endNode){
            // Visualisointia...
            highlightnode(closestNode + 'to' +parents[closestNode],'green');
            highlightnode(parents[closestNode] + 'to' +closestNode,'green');
            // Polun muodostaminen
            let path = new Array();
            // Aloitetaan lopusta
            let currentEnd = endNode;
            while (true) {
                // Lisätään polun alkuun
                path = [currentEnd].concat(path);
                // jos ollaan aloitusnodessa lopetetaan.
                if(currentEnd == startNode)
                    break;
                // haetaan noden vanhempi ja loopataan.
                currentEnd = parents[currentEnd];
            }
            // visualisoidaan polku
            for (let i = 1; i < path.length; i++) {
                highlight(path[i-1],path[i],0,'red');
            }
            return path;
        }
        let distanceFromLastFoundNode = Infinity;
        // käydään läpi löytämättömät nodet, päivitetään etäisyystaulua ja parent taulua niitten osalta
        for (var j = 0; j < nodeCount; j++)
        {
            if (!foundNodes[j])
            {
                if ((distanceFromLastFoundNode = distances[closestNode] + Matrix[closestNode][j]) < distances[j])
                {
                    distances[j] = distanceFromLastFoundNode;
                    parents[j] = closestNode;
                }
            }
        }
    }
    return distances;
}
// Highlightaa edgen ja noden halutulla värillä ja lisää nimeen =(cost) merkinnän
function highlight(from,node,cost,color = 'green')
{
    c++;
    setTimeout(function(){
        cy.$('#' + from + 'to' +node).data('color',color);
        cy.$('#' + node + 'to' +from).data('color',color);
        cy.$('#' + node).data('color',color);
        if(cost != 0)
        {
            cy.$('#' + node).data('name',cy.$('#' + node).data('name') + ' =' + cost);
        }
    }, speed*(c))
}
// Highlightaa noden halutulla värillä.
function highlightnode(node,color)
{
    c++;
    setTimeout(function(){
        cy.$('#' + node).data('color',color);
    }, speed*c);
}



