// Generoi vierusmatriisin satunnaisilla painoilla, mistä on helppo piirtää neliönmallinen esitys.
// parametrinä size, minkä "levyinen" neliöstä tulee size*size = nodejen määrä.
// myös mahdollista muuttaa minimi ja maksimi painoja.
function generateMatrix(size,minweight = 1, maxweight = 1000) {
    let nodeAmount = size*size;
    let floor = 0;
    var matrix = [];
    for (var i=0; i<nodeAmount; i++) {
        matrix.push(new Array(nodeAmount).fill(Infinity));
    }
    for (let i = 0; i < nodeAmount; i++) {

        if(i == floor * size - 1)
        {
            matrix[i][i-1] = matrix[i-1][i];
        }
        else if(i == floor * size)
        {
            matrix[i][i+1] = getRndInteger(minweight,maxweight);
            floor++;
        }
        else
        {
            matrix[i][i-1] = matrix[i-1][i];
            matrix[i][i+1] = getRndInteger(minweight,maxweight);
        }
        if(i < nodeAmount-size)
        {
            matrix[i][i+size] = getRndInteger(minweight,maxweight);
        }
        if(i >= size)
        {
            matrix[i][i-size] = matrix[i-size][i];
        }
    }
    return matrix;
}

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}
