﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace dijkstra
{
    class Dijkstra
    {
        public static int[] Run(ushort[][] mat, ushort startNode)
        {
            // Nodejen määrä = matriisin leveys/korkeus
            int nodeCount = mat.Length;
            // alustetaan algoritmit tarvitsevat taulut.
            bool[] foundNodes = new bool[nodeCount];
            // aloitusnode merkataan löydetyksi
            foundNodes[startNode] = true;
            int[] parents = new int[nodeCount];
            //etäisyydet vastaavat aloitusnoden painoja alkuun.
            int[] distances = Array.ConvertAll(mat[startNode], input => (int)input);// mat[startNode].ToArray();

            // etäisyys aloitukseen = 0
            distances[startNode] = 0;
            // merkataan ensimmästen polkujen vanhemmat
            for (var i = 0; i < nodeCount; i++)
            {
                if (mat[startNode][i] < Int16.MaxValue)
                    parents[i] = startNode;
            }
            // päälooppi
            for (var i = 0; i < nodeCount - 1; i++)
            {
                int distanceToClosestNode = Int32.MaxValue;
                ushort closestNode = UInt16.MaxValue;
                // tutkitaan löytämättömien nodejen etäisyydet ja valitaan lyhin.
                for (ushort j = 0; j < nodeCount; j++)
                {
                    if (!foundNodes[j] && distances[j] < distanceToClosestNode)
                    {
                        distanceToClosestNode = distances[j];
                        closestNode = j;
                    }
                }
                // merkataan löydetyksi
                foundNodes[closestNode] = true;
                int distanceFromLastFoundNode = Int32.MaxValue;
                // käydään läpi löytämättömät nodet, päivitetään etäisyystaulua ja parent taulua niitten osalta
                for (var j = 0; j < nodeCount; j++)
                {
                    if (!foundNodes[j])
                    {
                        if ((distanceFromLastFoundNode = (distances[closestNode] + mat[closestNode][j])) < distances[j])
                        {
                            distances[j] = distanceFromLastFoundNode;
                            parents[j] = closestNode;
                        }
                    }
                }
            }
            return distances;
        }
        
    }
}




