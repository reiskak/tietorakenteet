﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;

namespace dijkstra
{
    class Program
    {
        static void Main(string[] args)
        {
            // Loopataan eri generaatiokoita.
            for (int j = 10; j < 110; j += 10)
            {
                // ajanoton  tuloslista
                List<double> times = new List<double>();
                for (int i = 0; i < 100; i++)
                {
                    // Generoidaan vierusmatriisi
                    var mat = MatrixGenerator.GenerateMatrix(j);
                    // Aloitetaan C# tarkka ajastin
                    var sw = Stopwatch.StartNew();
                    // ajetaan algoritmi
                    Dijkstra.Run(mat, 0);
                    sw.Stop();
                    // lisätään aika tuloksiin
                    times.Add(sw.Elapsed.TotalMilliseconds);
                }
                // 100 ajon keskiarvo
                Console.WriteLine(j+ ": " + times.Average());
            }
        }

        // tällä voi testata vertailua varten minimikeko ja vieruslista versiota algoritmistä, joka on otettu https://rosettacode.org/wiki/Dijkstra%27s_algorithm#C.23
        static void TestAdjList()
        {
            List<double> times = new List<double>();
            int edges = 0;
            int nodes = 0;
            for (int j = 0; j < 100; j++)
            {
                var mat = MatrixGenerator.GenerateMatrix(90);
                var list = MatrixGenerator.convert(mat);
                Graph graph = new Graph(list.Count);
                int e = 0;
                for (int i = 0; i < list.Count; i++)
                {
                    foreach (var item in list[i])
                    {
                        e++;
                        graph.AddEdge(i, item.Item1, item.Item2);
                    }
                }

                nodes = list.Count;
                edges = e;
                var sw = Stopwatch.StartNew();
                var path = graph.FindPath(0);
                sw.Stop();
                times.Add(sw.Elapsed.TotalMilliseconds);
            }

            Console.WriteLine("nodes:" + nodes);
            Console.WriteLine("edges:" + edges);
            Console.WriteLine(times.Average());
        }

    }
}
