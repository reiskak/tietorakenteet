﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dijkstra
{
    // Generoi vierusmatriisin satunnaisilla painoilla, mistä on helppo piirtää neliönmallinen esitys.
    // parametrinä size, minkä "levyinen" neliöstä tulee size*size = nodejen määrä.
    // myös mahdollista muuttaa minimi ja maksimi painoja.
    class MatrixGenerator
    {
        public static ushort[][] GenerateMatrix(int size, int minweight = 1, int maxweight = 1000)
        {
            var rng = new Random();
            var nodenumber = size * size;
            var kerros = 0;
            ushort[][] mat = new ushort[nodenumber][];
            for (int i = 0; i < nodenumber; i++)
            {
                mat[i] = new ushort[nodenumber];
                Array.Fill(mat[i], UInt16.MaxValue);
            }

            for (int i = 0; i < nodenumber; i++)
            {
                for (int j = 0; j < nodenumber; j++)
                {

                    mat[i][j] = UInt16.MaxValue;

                }
            }

            for (int i = 0; i < nodenumber; i++)
            {
                if (i == kerros * size - 1)
                {
                    mat[i][i - 1] = mat[i - 1][i];
                }
                else if (i == kerros * size)
                {
                    mat[i][i + 1] = (ushort) rng.Next(minweight, maxweight);
                    kerros++;
                }
                else
                {
                    mat[i][i - 1] = mat[i - 1][i];
                    mat[i][i + 1] = (ushort) rng.Next(minweight, maxweight);
                }

                if (i < nodenumber - size)
                {
                    mat[i][i + size] = (ushort) rng.Next(minweight, maxweight);
                }

                if (i >= size)
                {
                    mat[i][i - size] = mat[i - size][i];
                }
            }

            return mat;
        }
        // Matriisin muuttaminen listaksi, adjacency lista version testaamista varten, https://www.geeksforgeeks.org/convert-adjacency-matrix-to-adjacency-list-representation-of-graph/
        public static List<List<(ushort, ushort)>> convert(ushort[][] a)
        {
            // no of vertices 
            int l = a.Length;
            List<List<(ushort, ushort)>> adjListArray = new List<List<(ushort, ushort)>>(l);

            // Create a new list for each 
            // vertex such that adjacent 
            // nodes can be stored 
            for (ushort i = 0; i < l; i++)
            {
                adjListArray.Add(new List<(ushort, ushort)>());
            }


            for (ushort i = 0; i < l; i++)
            {
                for (ushort j = 0; j < l; j++)
                {
                    if (a[i][j] < UInt16.MaxValue)
                    {
                        adjListArray[i].Add((j, a[i][j]));
                    }
                }
            }

            return adjListArray;
        }
    }
}
